DESTDIR=/usr/lib/python2.7
PROGFILE=libPrimes.py

install:
	cp $(PROGFILE) $(DESTDIR)
	pycompile $(DESTDIR)/$(PROGFILE)
	chmod 644 $(DESTDIR)/$(PROGFILE) $(DESTDIR)/$(PROGFILE)c

uninstall:
	rm -f $(DESTDIR)/$(PROGFILE)
	rm -f $(DESTDIR)/$(PROGFILE)c
