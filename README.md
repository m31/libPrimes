### ¿ Qué es libPrimes ?

libPrimes es una pequeña librería en python que implementa un test de primalidad
probabilístico. ¿ Cómo que probabilísico ?. Pues si, herramientas como gnupg, que
necesitan obtener números primos rápidamente, usan test probabilísticos. A día de
hoy no existen test determinísticos de primalidad lo suficientemente rápidos para
números grandes.

### ¿ Cúal es la probabilidad de error ?

Si mis cálculos no fallan ( seguro que sí xD ), la probabilidad de error es :

1 / 4^25 = 1 / 1125899906842624

Por supuesto, en la función del test de primalidad en python hay un argumento opcional
que es un array de bases a probar, que son números primos partiendo del 2. Si necesitas
un test mas rápido pero menos certero puedes reducir la colección de bases indicando
un array en el argumento opcional. Y al contrario, si quieres una seguridad mayor aún
si cabe, puedes pasarle a la función un array más grande.

### ¿ Cómo instalo la librería ?

Puedes optar por no instalarla, sencillamente teniendo la librería en el mismo
directorio que un programa que la use, o en todo caso en sus rutas de búsqueda.

Si quieres instalarla en tu sistema para importarla con mayor comodidad, tienes
que ejecutar como root el siguiente comando:

make install

Esto instalará la libreria en $DESTDIR y compilará el correspondiente fichero .pyc
Si quieres que se instale en otra ruta distinta, utiliza las posibilidades que make
nos brinda de la siguiente forma:

make DESTDIR=/usr/local/lib/python-libs-are-here install

### ¿ Cómo se usa la librería ?

Sencillamente tienes que importar la librería, y dispondrás de las siguientes
funciones ( en el ejemplo supongo que la has importado bajo el nombre "lp" )

lp.modExp(5,101,7)

Como su nombre indica, esta función efectúa la exponenciación modular ( si no
sabes lo que es esto no se por que cojones necesitas esta librería xD )

Todos los argumentos de dicha función han de ser enteros, y además tener sentido,
es decir, que el exponente sea estrictamente mayor que cero y el módulo mayor o
igual a dos. Si los argumentos son incorrectos, la función producirá un raise avisando
del error.

lp.isPrime(101)

Tal y como su nombre indica, esta función comprueba la primalidad de un número
con un test no determinístico. Hay un segundo argumento opcional que es un array
de números enteros y que han de ser primos consecutivos partiendo del 2 hasta la
base deseada. Por ejemplo:

lp.isPrime(101, (2,3,5))
