#!/usr/bin/python
# -*- coding: UTF-8 -*-

global_primedb = 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97

def modExp(base, exp, mod):

	if not type(42) == type(base) == type(exp) == type(mod):

		raise Exception("Error: modExp: Non-interger argument")

	if mod < 2 or exp < 0:

		raise Exception("Error: modExp: Invalid argument")

	ret = 1

	while exp != 0:

		if exp % 2 == 1:

			ret *= base
			ret %= mod

		exp >>= 1
		base = ( base ** 2 ) % mod

	return ret

def witnessPrimeCheck(witness, number):

	if not type(42) == type(witness) == type(number):

		raise Exception("Error: witnessPrimeCheck: Non-interger argument")

	if number % 2 == 0:

		raise Exception("Error: witnessPrimeCheck: Invalid argument")

	ret = modExp(witness, ( number - 1 ) >> 1, number)

	if ret == 1:

		return True

	elif ret == number - 1:

		return True

	else:

		return False

def eTest(number, primedb = global_primedb):

	if not type(42) == type(number):

		raise Exception("Error: eTest: Non-interger argument")

	for i in primedb:

		if i == number:

			return True

		if number % i == 0:

			return False

		if witnessPrimeCheck(i, number):

			continue

		else:

			return False

	return True
